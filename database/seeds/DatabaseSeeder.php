<?php
use App\User;
use App\Post;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 10)->create()->each(function(User $user){
            $user->foreignKey()->saveMany(
                factory(Post::class, random_int(1, 10))->make());
        });
    }
}
