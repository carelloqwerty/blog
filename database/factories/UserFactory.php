<?php
use App\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('secret'),
        //'$10$N2hPneUBl5za7Of1z.J0uuxdPJBR67hvYDTb4C5A4WUmOSGNuxuwS', // secret
        //не похожа стандартная строка на brcrypt('secret'), не логинит
        'remember_token' => str_random(10),
    ];
});
