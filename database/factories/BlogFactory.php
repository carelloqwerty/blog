<?php
use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    
    $image = $faker->image('public/uploads/images');
    $path = preg_replace('/public/', '', $image);
    $title = $faker->sentence;
    return [
        'slug' => str_slug($title),
        'title' => $title,
        'preview' => $faker->sentence,
        'text' => $faker->paragraphs(20, true),
        'image' => $path,
        'user_id' => null,
    ];
});
