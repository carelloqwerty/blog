@extends('layouts.app')

@section('content')
    <div class="row">
        @foreach($all as $post)
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="{{$post->image}}" alt="...">
                <div class="caption">
                  <h3>{{$post->title}}</h3>
                  <p>{{$post->preview_text}}</p>
                  <p><a href="{{route('detail',[$post->slug])}}" class="btn btn-primary" role="button">Detail</a></p>
                </div>
              </div>
            </div>
        @endforeach
    </div>

        {{ $all->links() }}
    </div>
@endsection