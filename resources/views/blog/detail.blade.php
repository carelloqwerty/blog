@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card mb-3">
            <img class="card-img-top" src="{{$info->image}}" alt="{{ $info->title}}">
                <div class="card-body">
                    <h1 class="card-title">{{ $info->title}}</h1>
                    <p class="card-text">{{ $info->text}}</p>
                    <a href="{{route('posts')}}" class="btn btn-primary">Go back</a>
                </div>
        </div>
    </div>
@endsection