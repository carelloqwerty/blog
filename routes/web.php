<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('posts', 'BlogController@index')->name('posts');
Route::get('posts/{post}', 'BlogController@show')->where('slug - это матюк', '[\w+\-]+')->name('detail');
//в фигурных скобках указывать строго имя таблицы
//Where 'slug' можно любой матюк писать
//если ищем по slug то нужно переопределить в методе getRouteKeyName() модели post
//иначе по умолчанию ищет по id
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
