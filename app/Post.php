<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $table = 'posts';
    protected $fillable = [
        'slug',
        'title',
        'preview',
        'text',
        'image',
        'user_id'
        ];
    
    public function getRouteKeyName() {
        return 'slug';
    }
//    public function foreignKey(){
//        
//        return $this->belongsTo(User::class, 'user_id', 'id' );
//    }
   
}
