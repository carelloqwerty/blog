<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(){
        $postList = Post::paginate(9);
        return view('blog.posts', [
            'all' => $postList
        ]);;
    }//
    
    public function show(Post $post)
    {
        return view('blog.detail', [
            'info' => $post
        ]);
    }
}
